<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            
            $data = [
                'view' => 'inicio',
                'page' => 'inicio'
            ];
		$this->load->view('template', $data);
	}
        
        public function alojamiento(){
            log_message('INFO', 'entra');
             $data = [
                'view' => 'alojamiento',
                'page' => 'alojamiento'
            ];
		$this->load->view('template', $data);
        }
        
         public function reservas(){
            log_message('INFO', 'entra');
             $data = [
                'view' => 'reservas',
                'page' => 'reservas'
            ];
		$this->load->view('template', $data);
        }
        
        public function contact(){
            log_message('INFO', 'entra');
             $data = [
                'view' => 'contact',
                'page' => 'contact'
            ];
		$this->load->view('template', $data);
        }
}
