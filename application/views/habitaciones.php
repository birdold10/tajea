<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php 
            require("navegadores.php") ;
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-escalable=no, initial-scalable=1.0, maximum-scalable=1.0, minimum-scalable=1.0">
        <link rel="stylesheet" href="css/bootstraps.css">
        <title>INICIO</title>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="index.php"><img src="img/logop.png" class="img-fluid" alt="Responsive image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="alojamiento.php">Alojamiento</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="reservas.php">reservas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php">Conocenos</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Buscador">
                        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
                    </form>
                </div>
            </nav>
        </header>            
        <div class="container">
            <div class="row">
              <!DOCTYPE html>
        <!--
            GALERIA DE IMAGENES CON BREVE DESCRIPCION
        
        -->  
            </div>    
        </div>
        <footer>
            <?php
                require("footer.php") ;
            ?>    
        </footer>
        <?php
        // put your code here
        ?>
        <script src="js/jquery.js"></script> 
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
