<div class="container" style="background-color:white">   
    <div class="row" >
        <div class="col-md-12 text-center">
            <img src="img/logog.png" class="img-fluid" alt="Responsive image">
        </div> 
    </div> 
</div>    
<div class="container">    
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="img/slide.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/slide2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="img/slide3.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row">
        <div class="col-md-4 text-center" style="padding-top: 10px">
            <img src="img/pareja.png" class="img-fluid" alt="Responsive image">
            <h4>capacidad</h4>
            <p>Disponemos de 6 habitaciones: 4 de ellas con cama de matrimonio y las otras 2 con dos camas individuales. Además dispone de un sofá cama de matrimonio.</p>
        </div>
        <div class="col-md-4 text-center" style="padding-top: 10px">
            <img src="img/cama.png" class="img-fluid" alt="Responsive image">
            <h4>habitaciones</h4>
            <p>Disponemos de 6 habitaciones: 4 de ellas con cama de matrimonio y las otras 2 con dos camas individuales. Además dispone de un sofá cama de matrimonio.</p>
        </div>
        <div class="col-md-4 text-center" style="padding-top: 40px">
            <img src="img/visa.png" class="img-fluid" alt="Responsive image" style="margin-bottom: 20px">
            <h4>tarifas</h4>
            <p>Disponemos de 6 habitaciones: 4 de ellas con cama de matrimonio y las otras 2 con dos camas individuales. Además dispone de un sofá cama de matrimonio.</p>
        </div>
    </div>
</div>