<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<div style="background-color:#714c17; padding-top: 20px; margin-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-md-4" style="padding-bottom: 20px">
                <h6><font color="white">CONTACTO</font></h6>
                <font color="white" size="2">Carretera Benamahoma - Ubrique</font>
                <font color="white" size="2">11600 - Ubrique (Cádiz). España.</font>
                <hr>
                <font color="white" size="2">GPS 36°39'46.232"N 5°26'57.943"W</font>
                <hr>
                <font color="white" size="2">Tel: +34 956 46 03 77</font>
                <font color="white" size="2">Movil: +34 615 59 07 77</font>
                <hr>
                <font color="white" size="2">info@casarura20pilares.com</font>
            </div>
            <div class="col-md-4">
                <h6><font color="white">LA TAJEA</font></h6>
                <a href="index.php"><font color="white" size="2">Inicio</font></a>
                <hr>
                <a href="alojamiento.php"><font color="white" size="2">Alojamiento</font></a>
                <hr>
                <a href="reservas.php"><font color="white" size="2">Reservas</font></a>
                <hr>
                <a href="contact.php"><font color="white" size="2">Contacto</font></a>
            </div>
            <div class="col-md-4">
                <h6><font color="white">PRIVACIDAD</font></h6>
                <a href="aviso.php"><font color="white" size="2">Aviso legal</font></a>
                <hr>
                <a href="terminos.php"><font color="white" size="2">Términos & condiciones</font></a>
            </div>
        </div>
    </div>
</div>
<div style="background-color:#412c0d; padding-top: 20px">
    <div class="container">    
        <div class="row">
            <div class="col-md-6 text-left">
                <p>© 2018 La tajea. Todos los derechos reservados.</p>
            </div>
            <div class="col-md-6 text-right">
                <p>Carretera de Cortes, Km. 2.</p>
            </div>
        </div>
    </div>
</div>
