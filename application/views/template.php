<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php 
//            require("navegadores.php") ;
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-escalable=no, initial-scalable=1.0, maximum-scalable=1.0, minimum-scalable=1.0">
        <link rel="stylesheet" href="http://localhost/tajea/public/css/bootstraps.css">
        <link rel="stylesheet" href="http://localhost/tajea/public/css/css.css">
        <title>INICIO</title>
    </head>
    <body>
        <header>
            <?php
                $this->load->view('header');
            ?>
        </header>           
        
        <div class="content">
            <?php $this->load->view($view); ?>
        </div>
        
        <footer>
            <?php
                $this->load->view('footer');
            ?>    
        </footer>
        <?php
        // put your code here
        ?>
<!--        <script src="js/jquery.js"></script> 
        <script src="js/bootstrap.min.js"></script>-->
    </body>
</html>
