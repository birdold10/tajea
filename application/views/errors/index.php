<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <?php 
            require("navegadores.php") ;
        ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-escalable=no, initial-scalable=1.0, maximum-scalable=1.0, minimum-scalable=1.0">
        <link rel="stylesheet" href="css/bootstraps.css">
        <title>INICIO</title>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="index.php"><img src="img/logop.png" class="img-fluid" alt="Responsive image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="alojamiento.php">Alojamiento</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="reservas.php">reservas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php">Conocenos</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Buscador">
                        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
                    </form>
                </div>
            </nav>
        </header>           
        <div class="container" style="background-color:white">   
            <div class="row" >
                <div class="col-md-12 text-center">
                    <img src="img/logog.png" class="img-fluid" alt="Responsive image">
                </div> 
            </div> 
        </div>    
        <div class="container">    
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="img/slide.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="img/slide2.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="img/slide3.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="row">
                <div class="col-md-4 text-center" style="padding-top: 10px">
                    <img src="img/pareja.png" class="img-fluid" alt="Responsive image">
                    <h4>capacidad</h4>
                    <p>Disponemos de 6 habitaciones: 4 de ellas con cama de matrimonio y las otras 2 con dos camas individuales. Además dispone de un sofá cama de matrimonio.</p>
                </div>
                <div class="col-md-4 text-center" style="padding-top: 10px">
                    <img src="img/cama.png" class="img-fluid" alt="Responsive image">
                    <h4>habitaciones</h4>
                    <p>Disponemos de 6 habitaciones: 4 de ellas con cama de matrimonio y las otras 2 con dos camas individuales. Además dispone de un sofá cama de matrimonio.</p>
                </div>
                <div class="col-md-4 text-center" style="padding-top: 40px">
                    <img src="img/visa.png" class="img-fluid" alt="Responsive image" style="margin-bottom: 20px">
                    <h4>tarifas</h4>
                    <p>Disponemos de 6 habitaciones: 4 de ellas con cama de matrimonio y las otras 2 con dos camas individuales. Además dispone de un sofá cama de matrimonio.</p>
                </div>
            </div>
        </div>
        <footer>
            <?php
                require("footer.php") ;
            ?>    
        </footer>
        <?php
        // put your code here
        ?>
        <script src="js/jquery.js"></script> 
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
