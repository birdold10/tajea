
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="index.php"><img src="img/logop.png" class="img-fluid" alt="Responsive image"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php echo $page == 'inicio' ? 'active' : ''; ?>">
                <a class="nav-link" href="<?php echo base_url("Welcome/index"); ?>">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url("Welcome/alojamiento"); ?>">Alojamiento</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url("Welcome/reservas"); ?>">reservas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url("Welcome/contact"); ?>">Conocenos</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Buscador">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
        </form>
    </div>
</nav>
