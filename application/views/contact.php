
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d688.7899274682046!2d-5.466103370796724!3d36.76293069874726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x58fdca5fdd8e65c8!2sEl+Abejaruco+Casa+Rural!5e1!3m2!1ses!2ses!4v1533724721339" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>  
            </div>
            <div class="row" >
                <div class="col-md-12" style="margin-top: 10px; margin-bottom: 5px">
                    <h5>Complemente el siguiente formulario para ponerse en contacto con nosotros:</h5>
                </div> 
            </div>
            <form action="" target="_blank">
                <div class="row" >
                    <div class="col-md-12">
                        <label>Nombre y apellidos</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="nombre">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label>Teléfono</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="telefono">
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12">
                        <label>Mensaje</label>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12">
                        <textarea style="margin-bottom: 15px" class="form-control" name="mensaje"></textarea>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-3">
                        <button class="btn btn-primary" value="Enviar mensaje">Enviar</button>
                    </div>
                </div>
            </form>  
        </div>
